package br.com.lead.collector.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
//@Table(name = "leads")
public class Lead
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    //@Column(name = "NM_LEAD")
    private String nome;
    //@Column(name = "CPF_LEAD")
    private String cpf;
    //@Column(name = "DS_EMAIL")
    private String email;
    //@Column (name = "DS_TEL")
    private String telefone;
    private LocalDate dataCriacao;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public Lead() {
    }
}
