package br.com.lead.collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
public class Produto
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message =  "Nome não pode ser vazio")
    @NotNull (message = "Nome deve ser informado")
    @Size(min = 5, message = "O nome deve ter pelo menos 5 caracteres")
    private String nome;

    @NotBlank(message =  "Descrição não pode ser vazia")
    @NotNull (message = "Descrição deve ser informada")
    @Size(min = 5, message = "A descrição deve ter pelo menos 5 caracteres")
    private String descricao;

    @NotNull (message = "Preço deve ser informado")
    @DecimalMin(value = "0", message = "Preço deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    private Double preco;

    private LocalDate DataDeCadastro;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public LocalDate getDataDeCadastro() {
        return DataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        DataDeCadastro = dataDeCadastro;
    }

    public Produto() {
    }
}
