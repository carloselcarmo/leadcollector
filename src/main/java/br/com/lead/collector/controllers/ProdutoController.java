package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto criar(@RequestBody @Valid Produto produto)
    {
        return produtoService.criar(produto);
    }

    @GetMapping
    public Iterable<Produto> buscarTodos()
    {
        return produtoService.buscarTodos();
    }

    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable(name = "id") Integer id)
    {
        try
        {
            return produtoService.buscarPorId(id);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizar(@PathVariable(name = "id") Integer id, @RequestBody @Valid Produto produto)
    {
        try
        {
            return produtoService.atualizar(id, produto);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable(name = "id") Integer id)
    {
        try
        {
            produtoService.excluir(id);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
