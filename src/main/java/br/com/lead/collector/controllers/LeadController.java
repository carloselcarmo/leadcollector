package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadController
{
    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead criar(@RequestBody @Valid CadastroDeLeadDTO lead)
    {
        return leadService.criar(lead);
    }

    @GetMapping
    public Iterable<Lead> buscarTodos()
    {
        return leadService.buscarTodos();
    }

    @GetMapping("/buscarTodosResumido")
    public List<ResumoDeLeadDTO> buscarTodosResumido()
    {
        return leadService.buscarTodosResumido();
    }

    @GetMapping("/buscarPorNome")
    public Lead buscarPorNome(@RequestParam(name = "nome") String nome)
    {
        try
        {
            return leadService.buscarPorNome(nome);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/buscarPorEmail")
    public Lead buscarPorEmail(@RequestParam(name = "email") String email)
    {
        try
        {
            return leadService.buscarPorEmail(email);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/buscarPorCpf")
    public Lead buscarPorCpf(@RequestParam(name = "cpf") String cpf)
    {
        try
        {
            return leadService.buscarPorCpf(cpf);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/buscarPorTelefone")
    public Lead buscarPorTelefone(@RequestParam(name = "telefone") String telefone)
    {
        try
        {
            return leadService.buscarPorTelefone(telefone);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/buscarPorProduto")
    public List<Lead> buscarPorProduto(@RequestParam(name = "idProduto") Integer idProduto)
    {
        return leadService.buscarPorProduto(idProduto);
    }

    @GetMapping("/{id}")
    public Lead buscarPorId(@PathVariable(name = "id") Integer id)
    {
        try
        {
            return leadService.buscarPorId(id);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizar(@PathVariable(name = "id") Integer id, @RequestBody @Valid Lead lead)
    {
        try
        {
            return leadService.atualizar(id, lead);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable(name = "id") Integer id)
    {
        try
        {
            leadService.excluir(id);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
