package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LeadRepository extends CrudRepository<Lead, Integer>
{
    Lead findFirstByCpf(String cpf);

    Lead findFirstByNome(String nome);

    Lead findFirstByEmail(String email);

    Lead findFirstByTelefone(String telefone);

    List<Lead> findByProdutosId(int id);
}
