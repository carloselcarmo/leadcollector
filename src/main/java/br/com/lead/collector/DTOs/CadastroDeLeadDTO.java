package br.com.lead.collector.DTOs;

import br.com.lead.collector.models.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class CadastroDeLeadDTO
{
    //@Column(name = "NM_LEAD")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser branco")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 caracteres")
    private String nome;
    //@Column(name = "CPF_LEAD")
    @CPF(message = "CPF é inválido")
    @NotNull(message = "CPF não pode ser nulo")
    private String cpf;
    //@Column(name = "DS_EMAIL")
    @Email(message = "E-mail é inválido")
    @NotNull(message = "E-mail não pode ser nulo")
    private String email;
    //@Column (name = "DS_TEL")
    private String telefone;

    @NotNull(message = "Produtos não pode ser nulo")
    private List<IdProdutoDTO> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<IdProdutoDTO> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<IdProdutoDTO> produtos) {
        this.produtos = produtos;
    }

    public CadastroDeLeadDTO() {
    }

    public Lead ConverterParaLead()
    {
        Lead lead = new Lead();
        lead.setCpf(this.cpf);
        lead.setEmail(this.email);
        lead.setNome(this.nome);
        lead.setTelefone(this.telefone);

        return lead;
    }
}
