package br.com.lead.collector.DTOs;

public class IdProdutoDTO
{
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IdProdutoDTO() {
    }

    public IdProdutoDTO(Integer id) {
        this.id = id;
    }
}
