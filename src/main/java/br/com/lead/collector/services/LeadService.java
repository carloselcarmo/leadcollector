package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService
{
    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead criar(CadastroDeLeadDTO lead)
    {
        Lead novolead = lead.ConverterParaLead();

        novolead.setDataCriacao(LocalDate.now());

        List<Integer> idProdutos = new ArrayList<>();
        for(IdProdutoDTO idprodutodto : lead.getProdutos())
        {
            Integer id = idprodutodto.getId();
            idProdutos.add(id);
        }

        Iterable<Produto> produtos = produtoRepository.findAllById(idProdutos);
        novolead.setProdutos((List<Produto>) produtos);

        return leadRepository.save((novolead));
    }

    public Iterable<Lead> buscarTodos()
    {
        return leadRepository.findAll();
    }

    public List<ResumoDeLeadDTO> buscarTodosResumido()
    {
        List<ResumoDeLeadDTO> lista = new ArrayList<>();
        for(Lead lead : leadRepository.findAll())
        {
            ResumoDeLeadDTO resumo = new ResumoDeLeadDTO();
            resumo.setEmail(lead.getEmail());
            resumo.setNome(lead.getNome());
            resumo.setId(lead.getId());
            lista.add(resumo);
        }

        return  lista;
    }

    public Lead buscarPorId(Integer id)
    {
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if(optionalLead.isPresent())
        {
            return optionalLead.get();
        }
        else
        {
            throw new RuntimeException("O Lead não foi encontrado");
        }
    }

    public Lead buscarPorNome(String nome)
    {
        Lead lead =leadRepository.findFirstByNome(nome);
        if(lead != null)
            return lead;
        else
            throw new RuntimeException("Nome não cadastrado");
    }

    public Lead buscarPorEmail(String email)
    {
        Lead lead =leadRepository.findFirstByEmail(email);
        if(lead != null)
            return lead;
        else
            throw new RuntimeException("Email não cadastrado");
    }

    public Lead buscarPorCpf(String cṕf)
    {
        Lead lead =leadRepository.findFirstByCpf(cṕf);
        if(lead != null)
            return lead;
        else
            throw new RuntimeException("CPF não cadastrado");
    }

    public Lead buscarPorTelefone(String telefone)
    {
        Lead lead =leadRepository.findFirstByTelefone(telefone);
        if(lead != null)
            return lead;
        else
            throw new RuntimeException("Telefone não cadastrado");
    }

    public List<Lead> buscarPorProduto(Integer id)
    {
        return leadRepository.findByProdutosId(id);
    }

    public Lead atualizar(Integer id, Lead lead)
    {
        Lead leaddb = buscarPorId(id);
        lead.setId((leaddb.getId()));
        return leadRepository.save(lead);
    }

    public void excluir(Integer id)
    {
        if(leadRepository.existsById(id))
        {
            leadRepository.deleteById(id);
        }
        else
        {
            throw new RuntimeException("O Lead não existe");
        }
    }
}
