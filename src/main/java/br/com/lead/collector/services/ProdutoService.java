package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto criar(Produto produto)
    {
        return produtoRepository.save((produto));
    }

    public Iterable<Produto> buscarTodos()
    {
        return produtoRepository.findAll();
    }

    public Produto buscarPorId(Integer id)
    {
        Optional<Produto> optionalProduto= produtoRepository.findById(id);
        if(optionalProduto.isPresent())
        {
            return optionalProduto.get();
        }
        else
        {
            throw new RuntimeException("O Produto não foi encontrado");
        }
    }

    public Produto atualizar(Integer id, Produto produto)
    {
        Produto produtodb = buscarPorId(id);
        produto.setId((produtodb.getId()));
        return produtoRepository.save(produto);
    }

    public void excluir(Integer id)
    {
        if(produtoRepository.existsById(id))
        {
            produtoRepository.deleteById(id);
        }
        else
        {
            throw new RuntimeException("O Produto não existe");
        }
    }
}
