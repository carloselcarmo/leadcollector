package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste
{
    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;

    @BeforeEach
    private void inicializar()
    {
        lead = new Lead();
        lead.setId(1);
        lead.setCpf("123465789");
        lead.setEmail("a@a.com");
        lead.setNome("Zé");
        lead.setDataCriacao(LocalDate.now());
        lead.setTelefone("12345-67896");

        produto = new Produto();
        produto.setId(1);
        produto.setDataDeCadastro(LocalDate.now());
        produto.setDescricao("aaaa");
        produto.setNome("Prod");
        produto.setPreco(1.0);

        List<Produto> produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void TestarbuscarPorIdEncontrandoLead()
    {
        //Preparação
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        //Execução
        Lead lead = leadService.buscarPorId(1);

        //Verificação
        Assertions.assertEquals(leadOptional.get(), lead);
    }

    @Test
    public void TestarbuscarPorIdLeadNaoEncontrado()
    {
        //Preparação
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarPorId(1);});
    }

    @Test
    public void Testarcriar()
    {
        //Preparação
        List<Produto> listaProdutos = new ArrayList<>();
        Produto produto1 = new Produto();
        produto1.setId(1);
        listaProdutos.add(produto1);
        Produto produto2 = new Produto();
        produto2.setId(2);
        listaProdutos.add(produto2);

        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("123456798");
        cadastroDeLeadDTO.setNome("Zé");
        cadastroDeLeadDTO.setCpf("123456789");
        cadastroDeLeadDTO.setEmail("a@a.com");

        List<IdProdutoDTO> listaIdProdutoDTO = new ArrayList<>();
        listaIdProdutoDTO.add(new IdProdutoDTO(1));
        listaIdProdutoDTO.add(new IdProdutoDTO(2));
        cadastroDeLeadDTO.setProdutos(listaIdProdutoDTO);

        Lead leadEsperado = cadastroDeLeadDTO.ConverterParaLead();
        leadEsperado.setProdutos(listaProdutos);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(listaProdutos);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

        //Execução
        Lead lead = leadService.criar(cadastroDeLeadDTO);

        //Verificação
        Assertions.assertEquals(leadEsperado.getNome(), lead.getNome());
        Assertions.assertEquals(leadEsperado.getProdutos(), lead.getProdutos());
    }

}
