package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTeste
{
    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    @Test
    public void TestarbuscarPorIdEncontrandoProduto()
    {
        //Preparação
        Produto produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.0);
        produto.setNome("camiseta");
        produto.setDescricao("descricao da camiseta");
        produto.setDataDeCadastro(LocalDate.now());

        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        //Execução
        Produto produtoCriado = produtoService.buscarPorId(1);

        //Verificação
        Assertions.assertEquals(produtoOptional.get(), produtoCriado);
    }

    @Test
    public void TestarbuscarPorIdProdutoNaoEncontrado()
    {
        //Preparação
        Optional<Produto> produtoOptional = Optional.empty();
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.buscarPorId(1);});
    }

    @Test
    public void TestarbuscarTodos()
    {
        //Preparação
        List<Produto> listaProdutos = new ArrayList<>();

        Produto produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.0);
        produto.setNome("camiseta");
        produto.setDescricao("descricao da camiseta");
        produto.setDataDeCadastro(LocalDate.now());
        listaProdutos.add(produto);

        Mockito.when(produtoRepository.findAll()).thenReturn(listaProdutos);

        //Execução
        Iterable<Produto> listaProdutosBuscados = produtoService.buscarTodos();

        //Verificação
        Assertions.assertEquals(listaProdutos, listaProdutosBuscados);
    }

    @Test
    public void Testarcriar()
    {
        //Preparação
        Produto produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.0);
        produto.setNome("camiseta");
        produto.setDescricao("descricao da camiseta");
        produto.setDataDeCadastro(LocalDate.now());

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        //Execução
        Produto produtoCriado = produtoService.criar(produto);

        //Verificação
        Assertions.assertEquals(produto, produtoCriado);
    }

    @Test
    public void Testaratualizar()
    {
        //Preparação
        Produto produtoAntigo = new Produto();
        produtoAntigo.setId(1);
        produtoAntigo.setPreco(10.0);
        produtoAntigo.setNome("camiseta");
        produtoAntigo.setDescricao("descricao da camiseta");
        produtoAntigo.setDataDeCadastro(LocalDate.now());

        Produto produtoNovo = new Produto();
        produtoNovo.setId(1);
        produtoNovo.setPreco(20.0);
        produtoNovo.setNome("camiseta");
        produtoNovo.setDescricao("descricao da camiseta");
        produtoNovo.setDataDeCadastro(LocalDate.now());

        Optional<Produto> produtoOptional = Optional.of(produtoAntigo);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoNovo);

        //Execução
        Produto produtoAtualizado = produtoService.atualizar(1, produtoNovo);

        //Verificação
        Assertions.assertEquals(produtoNovo.getPreco(), produtoAtualizado.getPreco());
    }

    @Test
    public void TestarexcluirEncontrandoProduto()
    {
        //Preparação
        Produto produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.0);
        produto.setNome("camiseta");
        produto.setDescricao("descricao da camiseta");
        produto.setDataDeCadastro(LocalDate.now());

        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);

        //Execução
        produtoService.excluir(1);

        //Verificação
        Mockito.verify(produtoRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void TestarexcluirSemEncontrarProduto()
    {
        //Preparação
        Produto produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.0);
        produto.setNome("camiseta");
        produto.setDescricao("descricao da camiseta");
        produto.setDataDeCadastro(LocalDate.now());

        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(false);

        //Mockito.when(produtoRepository.deleteById(Mockito.anyInt()));

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.excluir(1);});
        Mockito.verify(produtoRepository, Mockito.times(0)).deleteById(Mockito.anyInt());
    }
}
