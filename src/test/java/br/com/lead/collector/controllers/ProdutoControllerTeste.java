package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.jni.Local;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTeste
{
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void TestarbuscarPorId() throws Exception
    {
        Produto produto = new Produto();
        produto.setId(1);
        produto.setDataDeCadastro(LocalDate.now());
        produto.setDescricao("Camiseta");
        produto.setNome("Descição da camiseta");
        produto.setPreco(10.0);

        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void TestarbuscarPorIdSemResposta() throws Exception
    {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void TestarbuscarTodos() throws Exception
    {
        List<Produto> produtos = new ArrayList<>();
        Produto produto = new Produto();
        produto.setId(1);
        produto.setDataDeCadastro(LocalDate.now());
        produto.setDescricao("Camiseta");
        produto.setNome("Descição da camiseta");
        produto.setPreco(10.0);
        produtos.add(produto);

        Mockito.when(produtoService.buscarTodos()).thenReturn(produtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", Matchers.hasSize(1)));
    }

    @Test
    public void TestarbuscarTodosListaVazia() throws Exception
    {
        List<Produto> produtos = new ArrayList<>();

        Mockito.when(produtoService.buscarTodos()).thenReturn(produtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", Matchers.hasSize(0)));
    }

    @Test
    public void Testarcriar() throws Exception
    {
        Produto produto = new Produto();
        produto.setNome("Camiseta");
        produto.setDescricao("Descrição da camiseta");
        produto.setPreco(10.0);

        Mockito.when(produtoService.criar(Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .content(mapper.writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Camiseta")));

        Mockito.verify(produtoService, Mockito.times(1)).criar(Mockito.any(Produto.class));
    }

    @Test
    public void TestarcriarComErro() throws Exception
    {
        Produto produto = new Produto();
        produto.setNome("Camiseta");
        produto.setDescricao("");
        produto.setPreco(10.0);

        Mockito.when(produtoService.criar(Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .content(mapper.writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

        Mockito.verify(produtoService, Mockito.times(0)).criar(Mockito.any(Produto.class));
    }

    @Test
    public void Testaratualizar() throws Exception
    {
        Produto produto = new Produto();
        produto.setNome("Camiseta");
        produto.setDescricao("Descrição da camiseta");
        produto.setPreco(10.0);

        Mockito.when(produtoService.atualizar(Mockito.anyInt(), Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .content(mapper.writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(produtoService, Mockito.times(1)).atualizar(Mockito.anyInt(), Mockito.any(Produto.class));
    }

    @Test
    public void TestaratualizarComErro() throws Exception
    {
        Produto produto = new Produto();
        produto.setNome("Camiseta");
        produto.setDescricao("");
        produto.setPreco(10.0);

        Mockito.when(produtoService.criar(Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .content(mapper.writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

        Mockito.verify(produtoService, Mockito.times(0)).atualizar(Mockito.anyInt(), Mockito.any(Produto.class));
    }

    @Test
    public void Testarexcluir() throws Exception
    {
        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
