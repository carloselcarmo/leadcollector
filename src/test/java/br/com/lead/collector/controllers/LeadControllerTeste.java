package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste
{
    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;

    @BeforeEach
    private void Inicializar()
    {
        lead = new Lead();
        lead.setId(1);
        lead.setCpf("123465789");
        lead.setEmail("a@a.com");
        lead.setNome("Zezinho");
        lead.setDataCriacao(LocalDate.now());
        lead.setTelefone("12345-67896");

        produto = new Produto();
        produto.setId(1);
        produto.setDataDeCadastro(LocalDate.now());
        produto.setDescricao("aaaa");
        produto.setNome("Prod");
        produto.setPreco(1.0);

        List<Produto> produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void TestarbuscarPorId() throws Exception
    {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void TestarbuscarPorIdSemResposta() throws Exception
    {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void TestarbuscarTodos() throws Exception
    {
        List<Lead> leads = new ArrayList<>();
        leads.add(lead);
        leads.add(lead);

        Mockito.when(leadService.buscarTodos()).thenReturn(leads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", Matchers.hasSize(2)));
    }

    @Test
    public void TestarbuscarTodosListaVazia() throws Exception
    {
        List<Lead> leads = new ArrayList<>();

        Mockito.when(leadService.buscarTodos()).thenReturn(leads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", Matchers.hasSize(0)));
    }

    @Test
    public void Testarcriar() throws Exception
    {
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("123456798");
        cadastroDeLeadDTO.setNome("Zezinho");
        cadastroDeLeadDTO.setCpf("64003435044");
        cadastroDeLeadDTO.setEmail("a@a.com");

        List<IdProdutoDTO> listaIdProdutoDTO = new ArrayList<>();
        listaIdProdutoDTO.add(new IdProdutoDTO(1));
        listaIdProdutoDTO.add(new IdProdutoDTO(2));
        cadastroDeLeadDTO.setProdutos(listaIdProdutoDTO);

        Mockito.when(leadService.criar(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .content(mapper.writeValueAsString(cadastroDeLeadDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Zezinho")));

        Mockito.verify(leadService, Mockito.times(1)).criar(Mockito.any(CadastroDeLeadDTO.class));
    }

    @Test
    public void TestarcriarComErroNoCPF() throws Exception
    {
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("123456798");
        cadastroDeLeadDTO.setNome("Zezinho");
        cadastroDeLeadDTO.setCpf("11111111111");
        cadastroDeLeadDTO.setEmail("a@a.com");

        List<IdProdutoDTO> listaIdProdutoDTO = new ArrayList<>();
        listaIdProdutoDTO.add(new IdProdutoDTO(1));
        listaIdProdutoDTO.add(new IdProdutoDTO(2));
        cadastroDeLeadDTO.setProdutos(listaIdProdutoDTO);

        Mockito.when(leadService.criar(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .content(mapper.writeValueAsString(cadastroDeLeadDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
                //.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].field", CoreMatchers.equalTo("cpf")));

        Mockito.verify(leadService, Mockito.times(0)).criar(Mockito.any(CadastroDeLeadDTO.class));
    }
}
